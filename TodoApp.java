import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TodoApp {
    private static List<Task> todoList = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.println("1. Add Task");
            System.out.println("2. View Tasks");
            System.out.println("3. Search Tasks");
            System.out.println("4. Mark Task as Completed");
            System.out.println("5. Delete Task");
            System.out.println("6. Retrieve Task by Date");
            System.out.println("7. Exit");

            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    addTask();
                    break;
                case 2:
                    viewTasks();
                    break;
                case 3:
                    searchTasks();
                    break;
                case 4:
                    markTaskAsCompleted();
                    break;
                case 5:
                    deleteTaskByIndex();
                    break;
                case 6:
                    retrieveTaskByDate();
                    break;
                case 7:
                    System.out.println("Exiting application.");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
    }

    private static void addTask() {
        System.out.println("Enter task description:");
        String taskDescription = scanner.nextLine();

        System.out.println("Enter due date (YYYY-MM-DD):");
        String dateString = scanner.nextLine();
        LocalDate dueDate = LocalDate.parse(dateString);

        Task task = new Task(taskDescription, dueDate);
        todoList.add(task);
        System.out.println("Task added successfully!");
    }

    private static void viewTasks() {
        System.out.println("To-Do List");
        for (int i = 0; i < todoList.size(); i++) {
            Task task = todoList.get(i);
            System.out.println(i+1 + ". " + task);
        }
        System.out.println(" ");
    }

    private static void searchTasks() {
        System.out.println("Enter keyword to search:");
        String keyword = scanner.nextLine().toLowerCase();

        System.out.println("Search Results");
        boolean found = false;
        for (int i = 0; i < todoList.size(); i++) {
            Task task = todoList.get(i);
            if (task.getDescription().toLowerCase().contains(keyword)) {
                System.out.println(i + ". " + task);
                found = true;
            }
        }
        if (!found) {
            System.out.println("No matching tasks found.");
        }
        System.out.println(" ");
    }

    private static void markTaskAsCompleted() {
        System.out.println("Enter the index of the task to mark as completed:");
        int index = scanner.nextInt();

        if (index >= 0 && index < todoList.size()) {
            Task task = todoList.get(index);
            task.setCompleted(!task.isCompleted());
            System.out.println("Task marked as " + (task.isCompleted() ? "completed" : "not completed") + ": " + task.getDescription());
        } else {
            System.out.println("Invalid index. Please enter a valid index.");
        }
    }

    private static void deleteTaskByIndex() {
        viewTasks();
        System.out.println("Enter the index of the task to delete:");
        int index = scanner.nextInt();

        if (index >= 0 && index < todoList.size()) {
            Task deletedTask = todoList.remove(index-1);
            System.out.println("Task deleted successfully: " + deletedTask.getDescription());
        } else {
            System.out.println("Invalid index. Please enter a valid index.");
        }
    }

    private static void retrieveTaskByDate() {
        System.out.println("Enter due date of the task to retrieve (YYYY-MM-DD):");
        String dateString = scanner.nextLine();
        LocalDate dueDate = LocalDate.parse(dateString);

        boolean found = false;
        for (Task task : todoList) {
            if (task.getDueDate().equals(dueDate)) {
                System.out.println("Task for " + dueDate + ": " + task);
                found = true;
            }
        }
        if (!found) {
            System.out.println("No task found with the given due date.");
        }
    }
}

class Task {
    private String description;
    private LocalDate dueDate;
    private boolean completed;

    public Task(String description, LocalDate dueDate) {
        this.description = description;
        this.dueDate = dueDate;
        this.completed = false; // Tasks are initially not completed
    }

    public String getDescription() {

        return description;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return dueDate + " - " + description + " [" + (completed ? "Completed" : "Not Completed") + "]";
    }
}
